<!DOCTYPE html>
<html>
<head>
	<title>Tentukan nilai</title>
</head>
<body>
	<?php
	function tentukan_nilai($number)
	{
	    
	    if($number>=85){
	    	$nilai = "Sangat Baik<br>";
	    }

	    else if($number>=70 && $number<85){
	    	$nilai = "Baik<br>";
	    }

	    else if($number>=60 && $number<70){
	    	$nilai = "Cukup<br>";
	    }

	    else{
	    	$nilai = "Kurang<br>";
	    }

	    return $nilai;
	}

	echo tentukan_nilai(98); //Sangat Baik
	echo tentukan_nilai(76); //Baik
	echo tentukan_nilai(67); //Cukup
	echo tentukan_nilai(43); //Kurang
	?>

</body>
</html>